<?php
    /**
     * Created by PhpStorm.
     * User: ash
     * Date: 01/10/18
     * Time: 8:06 PM
     */
    require("../configFront.php");
    require("./application/SearchLookUp.php");
    require("./application/CommonClass.php");
    require("./application/LoadFormELements.php");

    /*
    * This page will list out all of form field elements.
    * @param {string} $serial_number ; is the serial number posted by the user from the search text box on the APP;
    */

    $LoadFormElements   = new LoadFormELements();
    $platformType       = $_GET['platformType'];

    if($platformType == NULL || $platformType == ""){
        $platformType   = 0;
    }

    $listConnectors     = $LoadFormElements->LoadAllConnectors($platformType);
    $listCables         = $LoadFormElements->LoadAllCables($platformType);
    $listJackets        = $LoadFormElements->LoadAllJacket($platformType);
    $listLength         = $LoadFormElements->LoadAllLength($platformType);
    $listPhaseMatch     = $LoadFormElements->LoadAllPhaseMatch($platformType);
    $listTestData       = $LoadFormElements->LoadAllTestData($platformType);

    $returnPack       = json_encode(array(
                                            "success"=>true,
                                            "connectorList"=>$listConnectors,
                                            "cableList"=>$listCables,
                                            "phaseMatchList"=>$listPhaseMatch,
                                            "lengthList"=>$listLength,
                                            "jacketList"=>$listJackets,
                                            "testData"=>$listTestData,
                                        )
                                    );
    print_r($returnPack);