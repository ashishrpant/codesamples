<?php
require_once 'database.class.php';

class Objects
{
    public static $single=0;
	 const SELECT=1;
	 const UPDATE=2;

	public static function list_attributes($tableName,$values=null)
		{
			$db = Db::getConnection();

			if(sizeof($values)!=0 )
			{
			$query="SELECT $values FROM $tableName";
			}
			else
			{
			$query="SELECT * FROM $tableName";
			}
			#echo $query;
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $resultSet;
			}
	public static function list_attributes_where($tableName,$values,$where)
		{
			$db = Db::getConnection();
			if(sizeof($values)!=0 )
			{
			$query="SELECT $values FROM $tableName $where";
			}
			else
			{
			$query="SELECT * FROM $tableName $where";
			}
			#echo $query."<br>";
			#die();
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if(static::$single==1)
            {
                return $resultSet[0];
                static::$single=0;
            }
			return $resultSet;
			}
    public static function list_join($tableName)
    {
        $db = Db::getConnection();

        $query="SELECT * FROM $tableName";

        #echo $query."<br>";
        #die();
        $stmt = $db->prepare($query);
        $stmt->execute();

        $resultSet = array();
        $resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if(static::$single==1)
        {
            return $resultSet[0];
            static::$single=0;
        }
        return $resultSet;
    }
	public static function list_distinct_attributes_where($tableName,$distinctValue,$where)
			{
			$db = Db::getConnection();

			$query="SELECT $distinctValue FROM $tableName $where";
		#	echo $query;
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $resultSet;
			}
			public static function list_distinct_attributes_where_array($tableName,$distinctValue,$where)
			{
				$db = Db::getConnection();
				$query="SELECT $distinctValue FROM $tableName $where";
				#echo $query;
				$stmt = $db->prepare($query);
				$stmt->execute();
				$resultSet = array();
				$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
				return $resultSet;
			}
			public static function rowCnt($tableName, $where=NULL)
			{
				$db = Db::getConnection();
				$query = "SELECT COUNT(*) AS cnt FROM $tableName $where";
				#echo $query."<br>";
				#die();
			#	$stmt = $db->query($query);
				$stmt = $db->prepare($query);
				$stmt->execute();
				$r = $stmt->fetchColumn();
				#echo $result->fetchColumn();
				$stmt->closeCursor();
				return $r;
			}
			public static function db_insert($table_name=null,$fields=null,$values=null)
			{
				$db = Db::getConnection();
				$field_name = "";
				$field_name2 = "";
				if(sizeof($fields) >0 )
				{
				for($i=0;$i<sizeof($fields);$i++)
				{
				$field_name.="`".$fields[$i]."`='".$values[$i]."',";
				}
				}
				$field_name=substr_replace($field_name,"",-1);
				$sql="INSERT INTO $table_name SET $field_name";
				#echo $sql."<br>";
				#die($sql);
				$q = $db->prepare($sql);
				$q->execute();
				$id = $db->lastInsertId();
				return $id;
			}

			public static function db_update($table_name=null,$fields=null,$values=null,$key,$val)
			{
				$db = Db::getConnection();
				$field_name = "";
				$field_name2 = "";
				if(sizeof($fields) >0 )
				{
				for($i=0;$i<sizeof($fields);$i++)
				{
				$field_name.="`".$fields[$i]."`='".$values[$i]."',";
				}
				}
				$field_name=substr_replace($field_name,"",-1);
				$sql="UPDATE $table_name SET $field_name WHERE $key='$val'";
				#echo $sql.'<br>';
				#die();
				$q = $db->prepare($sql);
				$q->execute();
				return $q;
			}
			public static function db_update_array($table_name=null,$fields=null,$values=null,$check,$key=null)
			{
				$db = Db::getConnection();
				if(sizeof($fields) >0 )
				{
					for($i=0;$i<sizeof($fields);$i++)
					{
						$field_name.="`".$fields[$i]."`='".$values[$i]."',";
					}
				}
				$field_name=substr_replace($field_name,"",-1);
				foreach($key as $id=>$val)
				{
					$sql="UPDATE $table_name SET $field_name WHERE $check = '$val'";
				#die($sql);
				#echo $sql;
					$q = $db->prepare($sql);
					$q->execute();

				}
				#echo $sql;
				#die();
				$parem = "executed";
				return $parem;
			}
			public static function db_update_unique($table_name=null,$fields=null,$key,$val)
			{
			$db = Db::getConnection();
			$field_name = "";
			$field_name2 = "";

			$sql="UPDATE $table_name SET $fields WHERE $key='$val'";
			#die();
			$q = $db->prepare($sql);
			$q->execute();
			return $q;
			}
	public static function getLogin_sup($table_name,$email,$password)
			{
			$db = Db::getConnection();
			$query=sprintf("SELECT * FROM `$table_name` WHERE supervisor='1' AND `emp_username`='%s' AND `emp_password`='%s'",mysql_escape_string($email),encryption_tag($password.SALT));
			#die($query);
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			#print_r($resultSet);
			return $resultSet;
			}
public static function getLogin_memb_1($table_name,$email)
			{
			$db = Db::getConnection();
			$query=sprintf("SELECT * FROM `$table_name` WHERE `emp_pin_code`='%s' ",mysql_escape_string($email));
			#die($query);
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			#print_r($resultSet);
			return $resultSet;
			}
public static function getLogin_supp_1($table_name,$email)
			{
			$db = Db::getConnection();
			$query=sprintf("SELECT * FROM `$table_name` WHERE ( supervisor='1' OR have_role='2') AND `emp_pin_code`='%s' ",mysql_escape_string($email));
			#ßdie($query);
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			#print_r($resultSet);
			return $resultSet;
			}
public static function getLogin_mem($table_name,$email,$pin)
			{
			$db = Db::getConnection();
			$query=sprintf("SELECT * FROM `$table_name` WHERE `emp_username`='%s' AND `emp_pin_code`='%s'",mysql_escape_string($email),($pin));
			#die($query);
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			#print_r($resultSet);
			return $resultSet;
			}
	public static function getLogin($table_name,$email,$password)
			{
			$db = Db::getConnection();
			$query=sprintf("SELECT * FROM `$table_name` WHERE `txtusername`='%s' AND `txtpassword`='%s'",mysql_escape_string($email),encryption_tag($password.SALT));
			#die($query);
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			#print_r($resultSet);
			return $resultSet;
			}
public static function getLogin_front_end($table_name,$email,$password)
			{
			$db = Db::getConnection();
			$query=sprintf("SELECT * FROM `$table_name` WHERE activation_flag='1' AND `user_name`='%s' AND `cust_password`='%s'",mysql_escape_string($email),encryption_tag($password.SALT));
			#die($query);
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			#print_r($resultSet);
			return $resultSet;
			}
	public static function forgotPswed($table_name)
			{
			$db = Db::getConnection();
			$query=sprintf("SELECT * FROM `$table_name` WHERE `usrNmea`='%s'",mysql_escape_string($_POST['us3rn4m3']));
			#die();
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			#print_r($resultSet);
			return $resultSet;
			}
	public static function getName($tableName,$values,$where)
		{
			$db = Db::getConnection();

			$query="SELECT $values FROM $tableName $where";
			#echo $query;
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $resultSet;
			}
public static function db_search($tableName,$where)
		{
			$db = Db::getConnection();

			$query="SELECT * FROM $tableName $where";
//			echo $query;
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			return $resultSet;
			}
public static function checkTracing($value)
			{
			$db = Db::getConnection();
			$query=sprintf("SELECT * FROM `tbl_tracingcodes` WHERE `tracingCodes`='%s' AND `expireDate`>='%s'",mysql_escape_string($value),date("m/d/Y"));
			#die($query);
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			#print_r($resultSet);
			return $resultSet;
			}
public static function db_incrment($table_name=null,$fields=null,$key,$val)
			{
			$db = Db::getConnection();
			$field_name = "";
			$field_name2 = "";
			$fields = $fields + 1;
			$sql="UPDATE $table_name SET $fields WHERE $key='$val'";
			$q = $db->prepare($sql);
			$q->execute();
			return $q;
			}
public static function db_incrment_param($table_name=null,$fields=null,$increaseBy,$key,$val)
			{
			$db = Db::getConnection();
			$field_name = "";
			$field_name2 = "";
			$fields = $fields + $increaseBy;
			echo $sql="UPDATE $table_name SET $fields WHERE $key='$val'";
			$q = $db->prepare($sql);
			$q->execute();
			return $q;
			}
public static function db_decrement($table_name=null,$fields=null,$increaseBy,$key,$val)
			{
			$db = Db::getConnection();
			$field_name = "";
			$field_name2 = "";
			$fields = $fields - 1;
			$sql="UPDATE $table_name SET $fields WHERE $key='$val'";
			$q = $db->prepare($sql);
			$q->execute();
			return $q;
			}
public static function db_decrement_param($table_name=null,$fields=null,$increaseBy,$key,$val)
			{
			$db = Db::getConnection();
			$field_name = "";
			$field_name2 = "";
			$fields = $fields - $increaseBy;
			echo $sql="UPDATE $table_name SET $fields WHERE $key='$val'";
			$q = $db->prepare($sql);
			$q->execute();
			return $q;
			}
public static function db_delete($table_name=null,$key,$val)
			{
			$db = Db::getConnection();
			$field_name = "";
			$field_name2 = "";
			#$fields = $fields + 1;
			$sql="DELETE FROM $table_name WHERE $key in(".implode(",",$val).")";
			#echo $sql;
			#die();
			$q = $db->prepare($sql);
			$q->execute();
			return $q;
			}
public static function db_delete_cart($table_name=null,$key,$val)
			{
			$db = Db::getConnection();
			$field_name = "";
			$field_name2 = "";
			#$fields = $fields + 1;
			$sql="DELETE FROM $table_name WHERE $key='$val'";
		#	echo $sql;
			#die("asdfasd");
			$q = $db->prepare($sql);
			$q->execute();
			return $q;
			}
public static function db_delete_cart_multiple($table_name=null,$val)
			{
			$db = Db::getConnection();
			$field_name = "";
			$field_name2 = "";
			#$fields = $fields + 1;
			$sql="DELETE FROM $table_name $val";
			#echo $sql;
			#die("asdfasd");
			$q = $db->prepare($sql);
			$q->execute();
			return $q;
			}
//forgot password
public static function checkEmail($table_name)
			{
			$db = Db::getConnection();
			$query=sprintf("SELECT * FROM `$table_name` WHERE `usrNmea`='%s'",mysql_escape_string($_POST['name']));
			#die($query);
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			#print_r($resultSet);
			return $resultSet;
			}
public static function checkEmail2($table_name)
			{
			$db = Db::getConnection();
			$query=sprintf("SELECT * FROM `$table_name` WHERE `emailAddSend`='%s'",mysql_escape_string($_POST['log']));
			#die();
			$stmt = $db->prepare($query);
			$stmt->execute();

			$resultSet = array();
			$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
			#print_r($resultSet);
			return $resultSet;
			}
            //added by samitrimal
            public static function query($query,$type=1)
            {
			$db = Db::getConnection();

			#echo $query."<br>";
			#die
			$stmt = $db->prepare($query);
			$stmt->execute();
			if($type==2)
			{
				return;
			}
        	if($type==1)
			{
				$resultSet = array();
				$resultSet = $stmt->fetchAll(PDO::FETCH_ASSOC);
				if(static::$single==1)
				{
					return $resultSet[0];
					static::$single=0;
				}
				return $resultSet;
			}
		}

            public static function update($tbl='',$data=array(),$where=array())
            {
                $sql="update $tbl set ";
                $data_arr=array();
                foreach ($data as $key=>$val )
                {
                    $data_arr[]="`$key`='$val'";
                }
                $sql.=implode(',',$data_arr);

                 $data_arr=array();
                foreach ($where as $key=>$val )
                {
                    $data_arr[]="`$key`='$val'";
                }
                $sql.=' WHERE 1 and';
                 $sql.=implode(' AND ' ,$data_arr);
              #  echo $sql;
                # exit;
                $db = Db::getConnection();
                $stmt=$db->prepare($sql);
                $stmt->execute();
                return $stmt;
            }
}
