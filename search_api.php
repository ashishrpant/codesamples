<?php



  require("../configFront.php");
  require("./application/SearchLookUp.php");
  require("./application/CommonClass.php");



  /*
  * This page will get the serial number from the API call.
  * @param {string} $serial_number ; is the serial number posted by the user from the search text box on the APP;
  */
   $SearchLookUp = new SearchLookUp();

  unset($_SESSION['config']);
  if($_POST['action']=='SEARCH_FUNCTION'){

      $serial_number = $_POST['serial_number'];
      $parsedSerialNumber = SearchLookUp::FormatSerialNumber($serial_number);
      $decodeSearialNumber = SearchLookUp::DecodeSearchNumber($parsedSerialNumber);

      /**
        * @param {JSON} $returned_param; holds all the parameters from the parsed serial number
        * @param integer $connector_id; is the connector id for the connector parsed from the serial number
        */
      $returned_param           = json_decode($decodeSearialNumber,TRUE);


      $returnPack       = json_encode(array(
              "success"=>true,
              "returned_params"=>$returned_param
          ));


        print_r($returnPack);


  }else{
    print_r(json_encode(array('success'=>false,"result"=>'fail')));
  }

 ?>
