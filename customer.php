<?php

/**
 * Created by PhpStorm.
 * User: apant
 * Date: 6/15/2016
 * Time: 1:24 PM
 * class Customer consists of all the functions used for CUSTOMER GUI and interfaces.
 */
class Customer
{
	private $db;
	public $result_val = array();
	public $result = "";

	public function __construct($db, $language_def)
	{
		$this->db = $db;
		//default language/
		$this->language_def = $language_def;
	}


	/**
	 * function fetchDashboardSystemLevel(); Get high-level counts of customers, carriers, vehicles
	 * @return string
	 */
	public function fetchDashboardSystemLevel()
	{
		$query = "CALL sp_fetch_dashboard_system_level();";
		return $this->db->fetchSingleRowStoredProcedure($query);
	}

	/**
	 * function fetchCustomerUuidFromIfId();Returns a customer_uuid or NULL/empty from customer_if_id
	 * @param $customer_if_id
	 * @return array
	 */
	public function fetchCustomerUuidFromIfId($customer_if_id )
	{
		$query = "call sp_fetch_customer_uuid_from_if_id('" . $customer_if_id . "', @var_out_row_count)";
		return $this->db->fetchSingleRowStoredProcedure($query);
	}
	/**
	 * function listCustomer(); Gets list of all customer on the database.
	 * @param null $sort_by
	 * @return array
	 */
	public function listCustomer($sort_by = NULL)
	{
		$query = "call sp_fetch_customer_get_all($sort_by,'$this->language_def', @var_out_row_count)";
		$this->result_val = array();
		if ($this->db->query($query) != false) {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
		}
		$this->db->nextResult();
		return $this->result_val;
	}

	/**
	 * function fetchCustomerDbNames(); Gets a listing of databases and respective customer_db_type_id for this customer, from a given customer_uuid
	 * If setup correctly, each customer shall have the following three databases:
	 * ELOG_CUST_DATA This customer-specific DB has the fully JSON transformed customer vehicle and driver records, and aggregated-reconciled records for submission to DOT.
	 * LMDC_CUST_DATA This customer-specific DB has the raw, parsed and fully JSON transformed data from each LMDirect Connector Listener daemon received message.
	 * This is for historical and future data-mining purposes.
	 * LMDC_LOG_DATA This customer-specific DB shall contain the parsing logs from each LMDirect Connector Listener daemon received message.
	 * The DOT ELD specification requires that such parsing logs are maintained.
	 * @param $customer_uuid
	 * @return array
	 */
	public function fetchCustomerDbNames($customer_uuid)
	{
		$query = "CALL sp_fetch_customer_get_db_names_from_uuid('" . $customer_uuid . "','$this->language_def',@var_out_row_count);";
		$this->result_val = array();
		if ($this->db->query($query) != false) {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
		}
		$this->db->nextResult();
		return $this->result_val;
	}

	/**
	 * function fetchAuditHistoryCustomer();Get audit history for a specific key, by date range
	 * @param $customer_uuid is the cusomter uuid of the customer whose history is being viewed
	 * @param $date_start is start date
	 * @param $date_end is end date
	 * @return array
	 */
	public function fetchAuditHistoryCustomer($customer_uuid, $date_start, $date_end)
	{

		$query = "call sp_fetch_audit_history_customer_from_uuid('" . $customer_uuid . "', '$date_start','$date_end','$this->language_def', @var_out_row_count)";
		$this->result_val = array();
		if ($this->db->query($query) != false) {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
			$this->db->nextResult();
		}
		return $this->result_val;

	}

	/**
	 * function fetchByUuid(); gets all the details of the customer which matches the customer UUID
	 * @param $customer_uuid is the unique identifier of the customer
	 * @return string
	 */
	public function fetchByUuid($customer_uuid)
	{
		$query = "CALL sp_fetch_customer_get_one_from_uuid('" . $customer_uuid . "','$this->language_def',@var_out_row_count);";
		return $this->db->fetchSingleRowStoredProcedure($query);
	}

	/**
	 * function fetchUstomerTypeCodeList();Get unique listing of customer_type_code
	 * @param null $sort_by
	 * @return array|bool
	 */
	public function fetchCustomerTypeCodelist($sort_by = NULL)
	{
		$query = "CALL sp_fetch_customer_type_code_list('$sort_by','$this->language_def',@var_out_row_count);";
		$this->result_val = array();
		$result = $this->db->query($query);
		if ($this->db->getRowCount($result) == FALSE) {
			$this->result_val = FALSE;
		} else {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
		}
		$this->db->nextResult();
		return $this->result_val;
	}

	/**
	 * function fetchUnitOfMeasure();Get unique listing of units_of_measure
	 * @param null $sort_by
	 * @return array|bool
	 */
	public function fetchUnitOfMeasure($sort_by = NULL)
	{
		$query = "CALL sp_fetch_units_of_measure_list('$sort_by','$this->language_def',@var_out_row_count);";
		$this->result_val = array();
		$result = $this->db->query($query);
		if ($this->db->getRowCount($result) == FALSE) {
			$this->result_val = FALSE;
		} else {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
		}
		$this->db->nextResult();
		return $this->result_val;
	}

	/**
	 * Procedure insertCustomerContractServiceType(); Inserts/associates a contract_service_type with a specific customer.
	 * @param $customer_uuid is the customer uuid where the contract will be applied.
	 * @param $contract_service_type_code is servicetype code
	 * @param $contract_service_pricing_type_code is price type code
	 * @param $contract_service_status_code is status code
	 * @param $contract_service_start_date is the service start date
	 * @param $number_of_licenses total no of licenses
	 * @param $license_unit_price is the unit price per license
	 * @param $varInLastModifiedAppName string is the name of the script page.
	 * @param $varInLastModfiedAppHost is the host.
	 * @param $varInLastModifiedAppUser string is the name of the person who's logged in currently.
	 * @return bool|string
	 */
	public function insertCustomerContractServiceType($customer_uuid, $contract_service_type_code, $contract_service_pricing_type_code, $contract_service_status_code, $contract_application_type_code,  $contract_service_start_date, $contract_service_start_date_tz,  $number_of_licenses, $license_unit_price, $varInLastModifiedAppName, $varInLastModfiedAppHost, $varInLastModifiedAppUser)
	{
		$insert_query = "call sp_insert_customer_contract_service_type('$customer_uuid',
			    														'$contract_service_type_code',
			    														'$contract_service_pricing_type_code',
			    														'$contract_service_status_code',
			    														'$contract_application_type_code',
                                                                        '$contract_service_start_date',
                                                                        '$contract_service_start_date_tz',
                                                                        '$number_of_licenses',
                                                                        '$license_unit_price',
                                                                        '$varInLastModifiedAppName',
                                                                        '$varInLastModfiedAppHost',
                                                                        '$varInLastModifiedAppUser',
                                                                        @var_out_uuid_inserted,
                                                                        @var_out_rows_affected)";
	#	echo $insert_query;
		$insert_recrord_result = $this->db->query($insert_query);
		if ($insert_recrord_result == true) {
			$this->result = true;
		} else {
			$this->result = false;
		}
		$this->db->nextResult();
		return $this->result;
	}

	/**
	 * PROCEDURE sp_update_customer_contract_service_type_by_uuid();Updates a specific customer/contract_service_type record/association, by respective customer_to_contract_service_type_uuid
	 * @param $service_type_id
	 * @param $contract_service_type_code
	 * @param $contract_service_pricing_type_code
	 * @param $contract_service_status_code
	 * @param $contract_service_start_date
	 * @param $number_of_licenses
	 * @param $license_unit_price
	 * @param $varInLastModifiedAppName string is the name of the script page.
	 * @param $varInLastModfiedAppHost is the host.
	 * @param $varInLastModifiedAppUser string is the name of the person who's logged in currently.
	 * @return bool|string
	 */
	public function updateCustomerContractServiceType($service_type_id, $contract_service_pricing_type_code, $contract_service_status_code, $contract_application_type_code,  $contract_service_start_date, $contract_service_start_date_tz, $number_of_licenses, $license_unit_price, $varInLastModifiedAppName, $varInLastModfiedAppHost, $varInLastModifiedAppUser)
	{
		$insert_query = "call sp_update_customer_contract_service_type_by_uuid('$service_type_id',
			    														'$contract_service_pricing_type_code',
			    														'$contract_service_status_code',
			    														'$contract_application_type_code',
                                                                        '$contract_service_start_date',
                                                                        '$contract_service_start_date_tz',
                                                                        '$number_of_licenses',
                                                                        '$license_unit_price',
                                                                        '$varInLastModifiedAppName',
                                                                        '$varInLastModfiedAppHost',
                                                                        '$varInLastModifiedAppUser',
                                                                        @var_out_rows_affected)";
		# die($insert_query);

		$insert_recrord_result = $this->db->query($insert_query);
		if ($insert_recrord_result == true) {
			$this->result = true;
		} else {
			$this->result = false;
		}
		$this->db->nextResult();
		return $this->result;
	}
	public function fetchContractApplicationTypes($sort_by = NULL)
	{

		$query = "call sp_fetch_contract_application_types($sort_by,'$this->language_def', @var_out_row_count)";
		$this->result_val = array();
		if ($this->db->query($query) != false) {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
			#$db->closeResult();;
			$this->db->nextResult();
		}
		return $this->result_val;
	}
	/**
	 * function customerContractServiceTypesGetOne(); Gets a single customer -to- contract_service_types record by respective customer_to_contract_service_type_uuid
	 * GUI, when selecting a pricing type from list, should indicate divergence from the default_contract_service_pricing_type_code, displaying a GREEN 'Default Pricing" or RED "Not Default Pricing"
	 * license_number and vehicle_active_license_count should be shown as label. Indicate GREEN or RED cell for above license usage
	 * @param $var_in_customer_to_contract_service_type_uuid
	 * @return string
	 */
	public function fetchCustomerContractServiceTypesGetOne($var_in_customer_to_contract_service_type_uuid)
	{
		$query = "CALL sp_fetch_customer_contract_service_type_get_one('" . $var_in_customer_to_contract_service_type_uuid . "','$this->language_def',@var_out_row_count);";
		if ($this->db->query($query) != false) {
			$this->result = $this->db->nextArrayRow();
		}
		$this->db->nextResult();
		return $this->result;
	}

	/**
	 * function fetchContractServiceTypes();   Listing of contract_service_types that can be associated to Customers or Vehicles.
	 * This will only display Contract Service Types that have a show_in_gui NOT-EQUAL to 0. Therefore, we can hold back service types in development.
	 * @param BINARY $sort_by
	 * @return string
	 */
	public function fetchContractServiceTypes($sort_by = NULL)
	{
		$query = "CALL sp_fetch_contract_service_types('" . $sort_by . "','$this->language_def',@var_out_row_count);";
		$this->result_val = array();
		$result = $this->db->query($query);
		if ($this->db->getRowCount($result) == FALSE) {
			$this->result_val = FALSE;
		} else {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
		}
		$this->db->nextResult();
		return $this->result_val;
	}

	/**
	 * function fetchContractServicePricingTypes();Listing of contract_service_pricing_types that are to be associated with vehicle contract_service_type associations.
	 * The contract_service_pricing_types at the Customer-level is the default for all child vehicle associations.
	 * @param null $sort_by
	 * @return array|bool
	 */
	public function fetchContractServicePricingTypes($sort_by = NULL)
	{
		$query = "CALL sp_fetch_contract_service_pricing_types('" . $sort_by . "','$this->language_def',@var_out_row_count);";
		$this->result_val = array();
		$result = $this->db->query($query);
		if ($this->db->getRowCount($result) == FALSE) {
			$this->result_val = FALSE;
		} else {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
		}
		$this->db->nextResult();
		return $this->result_val;
	}

	/**
	 * function fetchContractServiceStatusList(); Listing of contract_service_status_codes
	 * The LMDirectConnector daemon has various integrations, such as ENGINE_BUS,
	 * that will not be processed unless the corresponding contract status code is ACTIVE for the CUSTOMER and VEHICLE
	 * @param null $sort_by
	 * @return array|bool
	 */
	public function fetchContractServiceStatusList($sort_by = NULL)
	{
		$query = "CALL sp_fetch_contract_service_status_list('" . $sort_by . "','$this->language_def',@var_out_row_count);";
		$this->result_val = array();
		$result = $this->db->query($query);
		if ($this->db->getRowCount($result) == FALSE) {
			$this->result_val = FALSE;
		} else {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
		}
		$this->db->nextResult();
		return $this->result_val;
	}

	/**
	 * call sp_fetch_customer_contract_service_types_from_cust_uui(); Gets a listing of all contract_service_types and the current status of these at the customer-level,
	 * by respective customer_uuid.
	 * @param $customer_uuid is the customer uuid
	 * @return array|bool
	 */
	public function customerContractServiceTypesFromCustUuid($customer_uuid)
	{
		$query = "call sp_fetch_customer_contract_service_types_from_cust_uuid('" . $customer_uuid . "','$this->language_def', @var_out_row_count)";
		$this->result_val = array();
		$result = $this->db->query($query);
		if ($this->db->getRowCount($result) == FALSE) {
			$this->result_val = FALSE;
		} else {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
		}
		$this->db->nextResult();
		return $this->result_val;
	}

	/**
	 * call sp_fetch_carrier_vehicle_group_list_for_customer(); Fetches all vehicle groups existing for a customer
	 * @param $customer_uuid is the customer uuid
	 * @return array|bool
	 */
	public function carrierVehicleGroupListForCustomer($customer_uuid)
	{
		$query = "call sp_fetch_carrier_vehicle_group_list_for_customer('" . $customer_uuid . "','$this->language_def', @var_out_row_count)";
		$this->result_val = array();
		$result = $this->db->query($query);
		if ($this->db->getRowCount($result) == FALSE) {
			$this->result_val = FALSE;
		} else {
			while (($rowArr = $this->db->nextArrayRow()) != false) {
				$this->result_val[] = $rowArr;
			}
		}
		$this->db->nextResult();
		return $this->result_val;
	}
	/**
	 * call sp_fetch_customer_tab_totals(); Get totals for tabbed-window display.
	 * Returns
	 * total_active_carriers,
	 * total_active_vehicles,
	 * total_active_drivers,
	 * total_active_other_users
	 * total_active_CUSTOMER_ADMIN_users
	 * total_active_CUSTOMER_VIEW_users
	 * total_physical_address_records
	 * total_contact_phone_records
	 * total_contact_email_records
	 * @param $customer_uuid
	 * @return array|bool
	 */
	//gets the tabs on the customer profile page
	public function customerTabTotals($customer_uuid)
	{
		$query = "call sp_fetch_customer_tab_totals('" . $customer_uuid . "','$this->language_def', @var_out_row_count)";
		$this->result_val = array();
		$result = $this->db->query($query);
		if ($this->db->getRowCount($result) == FALSE) {
			$this->result_val = FALSE;
		} else {
			$this->result_val = $this->db->nextArrayRow();
		}
		$this->db->nextResult();
		return $this->result_val;
	}

	//insert customer
	/**
	 * @param $varInStatusCode
	 * @param $varInProductionCode
	 * @param $varInCustomerTypeCode
	 * @param $varInCustomerIsIf
	 * @param $varInCustomerId
	 * @param $varInCustomerIfUrl
	 * @param $varInCustomerAliasName
	 * @param $varInCustomerName
	 * @param $varInUnitsOfMeasurementId
	 * @param $varInLastModifiedAppName string is the name of the script page.
	 * @param $varInLastModfiedAppHost is the host.
	 * @param $varInLastModifiedAppUser string is the name of the person who's logged in currently.
	 * @return bool|string
	 */
	public function insertCustomer($varInStatusCode, $varInProductionCode, $varInCustomerTypeCode, $varInCustomerIsIf, $varInCustomerId, $varInCustomerIfUrl, $varInCustomerAliasName, $varInCustomerName, $varInUnitsOfMeasurementId, $varInLastModifiedAppName, $varInLastModfiedAppHost, $varInLastModifiedAppUser)
	{
		$insert_query = "call sp_insert_customer('$varInStatusCode',
				 									 '$varInProductionCode',
													 '$varInCustomerTypeCode',
													 '$varInCustomerIsIf',
													 '$varInCustomerId',
													 '$varInCustomerIfUrl',
													 '$varInCustomerAliasName',
													 '$varInCustomerName',
													 '$varInUnitsOfMeasurementId',
													 '$varInLastModifiedAppName',
													 '$varInLastModfiedAppHost',
													 '$varInLastModifiedAppUser',
													  @var_out_uuid_inserted,
													  @var_out_rows_affected)";
		$update_record_query = $this->db->query($insert_query);
		if ($update_record_query == true) {
			$this->result = true;
		} else {
			$this->result = false;
		}
		$this->db->nextResult();
		return $this->result;
	}

	/**
	 * function updateCustomer(); Update an existing customer record
	 * @param $cust_uuid customer uuid of existing customer whose record is being edited
	 * @param $varInCustomerAliasName ; customer alias name
	 * @param $varInCustomerName ; customer name
	 * @param $varInUnitsOfMeasurementId ; unit of measure for customer
	 * @param $varInLastModifiedAppName string is the name of the script page.
	 * @param $varInLastModfiedAppHost is the host.
	 * @param $varInLastModifiedAppUser string is the name of the person who's logged in currently.
	 * @return bool|string
	 */
	public function updateCustomer($cust_uuid, $varInCustomerAliasName, $varInCustomerName, $varInUnitsOfMeasurementId, $varInLastModifiedAppName, $varInLastModfiedAppHost, $varInLastModifiedAppUser)
	{
		$insert_query = "call sp_update_customer('$cust_uuid',
                                                     '$varInCustomerAliasName',
													 '$varInCustomerName',
													 '$varInUnitsOfMeasurementId',
													 '$varInLastModifiedAppName',
													 '$varInLastModfiedAppHost',
													 '$varInLastModifiedAppUser',
													  @var_out_rows_affected);";
		$update_record_query = $this->db->query($insert_query);
		if ($update_record_query == true) {
			$this->result = true;
		} else {
			$this->result = false;
		}
		$this->db->nextResult();
		return $this->result;
	}

	/**
	 * function updateCustomerIfId(); Updates the Intellifuel Portal-InCab IF ID status, ID, and URL
	 * @param $cust_uuid
	 * @param $varInCustomerIsIf
	 * @param $varInCustomerId
	 * @param $varInCustomerIfUrl
	 * @param $varInLastModifiedAppName
	 * @param $varInLastModfiedAppHost
	 * @param $varInLastModifiedAppUser
	 * @return bool|string
	 */
	public function updateCustomerIfId($cust_uuid, $varInCustomerIsIf, $varInCustomerId, $varInCustomerIfUrl, $varInLastModifiedAppName, $varInLastModfiedAppHost, $varInLastModifiedAppUser)
	{
		$insert_query = "call sp_update_customer_if_id('$cust_uuid',
                                                     '$varInCustomerIsIf',
													 '$varInCustomerId',
													 '$varInCustomerIfUrl',
													 '$varInLastModifiedAppName',
													 '$varInLastModfiedAppHost',
													 '$varInLastModifiedAppUser',
													  @var_out_rows_affected);";
		$update_record_query = $this->db->query($insert_query);
		if ($update_record_query == true) {
			$this->result = true;
		} else {
			$this->result = false;
		}
		$this->db->nextResult();
		return $this->result;
	}

	/**
	 * function updateCustomerStatusType();Updates the customer status, type, and production state.
	 * @param $cust_uuid
	 * @param $varInStatusCode
	 * @param $varInProductionCode
	 * @param $varInCustomerTypeCode
	 * @param $varInLastModifiedAppName
	 * @param $varInLastModfiedAppHost
	 * @param $varInLastModifiedAppUser
	 * @return bool|string
	 */
	public function updateCustomerStatusType($cust_uuid, $varInStatusCode, $varInProductionCode, $varInCustomerTypeCode, $varInLastModifiedAppName, $varInLastModfiedAppHost, $varInLastModifiedAppUser)
	{
		$insert_query = "call sp_update_customer_status_and_type('$cust_uuid',
                                                     '$varInStatusCode',
													 '$varInProductionCode',
													 '$varInCustomerTypeCode',
													 '$varInLastModifiedAppName',
													 '$varInLastModfiedAppHost',
													 '$varInLastModifiedAppUser',
													  @var_out_rows_affected);";
		$update_record_query = $this->db->query($insert_query);
		if ($update_record_query == true) {
			$this->result = true;
		} else {
			$this->result = false;
		}
		$this->db->nextResult();
		return $this->result;
	}
}
