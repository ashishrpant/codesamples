/**
 * Created by apant on 9/30/2016.
 * Driver class consists of all the triggers on the Landing page
 * function EditProfile is used to edit profile of the driver
 * @param {string} drive_type_name   -   Is an array with all the DRIVER USE TYPES
 * */

var url = "/driver/api/";
var image_url = "/resources/img/";
var DriverInitialize;



class Driver {
	/**
	 * EditPicture function is triggered when  user clicks on edit picture link on the web browser.
	 * @param user_uuid is the elog_user_uuid
	 * @param image_id is the image_uuid of the image that already exists; its NULL if no uuid
	 */
	EditPicture() {
		var user_uuid 		= 	$("#edit_picture").attr("user_uuid");
		var image_id 		= 	$("#edit_picture").attr("picture_uuid");
		var BootstrapDialogOrderDetail = BootstrapDialog.show({
			title: 'Change your picture',
			message: $('<div></div>').load(url+'update-image-user.php?user_uuid=' +user_uuid+'&image_id='+image_id),
			onshown:function(dialogRef){ },
			size: BootstrapDialog.SIZE_WIDE,
			draggable: true,
			onhidden: function(dialogRef){
				$("body").removeClass("modal-open");
				BootstrapDialogOrderDetail = null;

				$(".modal-dialog").hide();
			}
		});
	}
	/**
	 * EditProfile function is triggered when Driver clicks on the edit profile link on the web browser
	 * @param user_uuid is the elog_user_uuid
	 */
	EditProfile() {
		var user_uuid 		= 	$("#edit_profile").attr("user_uuid");
		var BootstrapDialogOrderDetail = BootstrapDialog.show({
			title: 'Edit your profile',
			message: $('<div></div>').load(url+'update-profile-user.php?user_uuid=' +user_uuid),
			onshown:function(dialogRef){ },
			size: BootstrapDialog.SIZE_WIDE,
			draggable: true,
			onhidden: function(dialogRef){
				$("body").removeClass("modal-open");
				BootstrapDialogOrderDetail = null;

				$(".modal-dialog").hide();
			}
		});
	}

	/**
	 * Triggers when user's not online and tries to assign and NEW VEHICLE to himself with MODE and USE TYPE
	 * OnDuty Function triggers when DRIVER clicks ON_DUTY link on the web browser
	 */

	OnDuty(){
		$("#list_v").show();
        /*
         * WHEN users select the vehicle from the drop down list/
         * LoadVehicle FUNCTION loads a function which calls another function which lists all the vehicles based on the DRIVERS'S VEHICLE_GROUP OR CARRIER_GROUP
         */

		this.LoadVehicle( function (response){
			var data=response.replace(/^\s*/, '').replace(/\s*$/, '');
			if(data!='fail'){
				$( "#vehicle_select_list" ).html( data );
				//console.log("Hello");
			}else{
				$( "#js_error" ).show();
				console.log("Hello");
			}
		});

	}

	/**
	 * LoadVehicle FUNCTION loads a lists all the vehicles based on the DRIVERS'S VEHICLE_GROUP OR CARRIER_GROUP
	 */

	LoadVehicle(success){
		var request = $.ajax({
			type:"POST",
			url: url+"getVehicleList.php",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			// data: {vehicle_uuid:vehicle_uuid,cur_drvr_assig_uuid:cur_drvr_assig_uuid,carr_uuid:carr_uuid}

		});
		request.done(function(response) {
			var vehicle_list_parem      = "";
			var total_no_drivers        = "";
			var vehicle_info            = "";
			var image_holder            = "";
			var text_hold               = "";
			var i                       = 0;
			var vehi_uuid               = "";
			var carr_uuid               = "";

			$.each(response["vehicle_current_info"], function(ind, vehInfo) {

				total_no_drivers = vehInfo["vehicle_current_driver_assignment_total"];

				switch(total_no_drivers){
					case '0':
						image_holder = 'no_image_def.png';
						text_hold = "NOT IN USE";
						break;
					case '1':
						image_holder = 'driving.png';
						text_hold = "IN USE";
						break;
					default :
						image_holder = 'group_driving.png';
						text_hold = "IN USE";
						break;
				}
				vehi_uuid = vehInfo["vehicle_uuid"];
				carr_uuid = vehInfo["carrier_uuid"];
				//console.log(total_no_drivers);
				//image_holder = 'group_driving.png';
				//console.log('total no drivers'+total_no_drivers+);
				vehicle_info = vehicle_info+'<li class="veh_list" id="'+i+'"   ><a onclick=DriverInitialize.LoadMode("'+vehi_uuid+'","'+carr_uuid+'")  href="javascript:void(0);" vehicle_uuid="'+vehInfo["vehicle_uuid"]+'"><span cur_drvr_assig_uuid="'+vehInfo["vehicle_current_driver_assignment_total"]+'" vehicle_uuid="'+vehInfo["vehicle_uuid"]+'" carr_uuid="'+vehInfo["carrier_uuid"]+'" vehicle_current_driver_assignment_total="'+vehInfo["vehicle_current_driver_assignment_total"]+'"></span></span><img src=\''+image_url+image_holder+'\' width=\'20\'/> '+' &nbsp;'+text_hold+' &nbsp;'+vehInfo["vehicle_alias_id"]+' { '+vehInfo["carrier_name"]+' }</a></li>';
				i++; });

			var vehicle_list_parem = "<div class='input-group-btn select' id='vehicle_list'><button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-expanded='false'><span class='selected'>Vehicle Status / Vehicle No { Carrier Name }</span> <span class='caret'></span></button><ul class='dropdown-menu option' id='ul_veh_list'>"+vehicle_info+"</ul></div>";

			success(vehicle_list_parem);

		});

		request.fail(function( jqXHR, textStatus ) {
			alert( "Request failed: " + textStatus );
		});



	}
	/**
	 * LoadMode FUNCTION loads a function calls LoadModeList which lists all the modes
	 */

	LoadMode(vehicle_uuid,carr_uuid){

		this.LoadModeList(vehicle_uuid,carr_uuid,function (response){
			var data=response.replace(/^\s*!/, '').replace(/\s*$/, '');

			if(data!='fail'){
				$("#vehicle_only a span").attr("vehicle_uuid",vehicle_uuid);
				$("#vehicle_only a span").attr("carr_uuid",carr_uuid);
				$( "#vehicle_select_list" ).html( data );
				$( "#on_line_only").hide();
				$( "#vehicle_only" ).show();
			}else{
				$( "#js_error" ).show();
				//console.log("Hello");
			}
		});
	}

	LoadModeList(vehicle_uuid,carr_uuid,success)    {
		var val_class                   = "";
		var val_checked                 = "";
		var cur_drvr_assig_uuid         = $('#vehicle_list ul li.veh_list').attr("cur_drvr_assig_uuid");

		var request = $.ajax({
			type:"POST",
			url: url+"mode_select.php",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			data: {vehicle_uuid:vehicle_uuid,cur_drvr_assig_uuid:cur_drvr_assig_uuid,carr_uuid:carr_uuid}

		});

		request.done(function(response) {
			var i =0;
			var drive_type_name = "";

			//console.log(response["drive_mode_list"]);
			$.each(response["drive_mode_list"], function(ind, modeName) {
				val_class = " btn-default";
				drive_type_name = drive_type_name+'<label class="btn '+val_class+' mode_type_only"><input type="radio" onchange="DriverInitialize.LoadUserType(\''+modeName["driver_mode_code"]+'\')" cur_drvr_assig_uuid="'+cur_drvr_assig_uuid+'" vehicle_uuid="'+vehicle_uuid+'" carr_uuid="'+carr_uuid+'" driver_mode_code="'+modeName["driver_mode_code"]+'"  name="driver_status" '+val_checked+' value="'+modeName["driver_mode_code"]+'"/>'+modeName["driver_mode_name"]+"</label>";
				i++;
			});
			//console.log(drive_type_name);
			var input_parem = "<div class='btn-group' data-toggle='buttons' id='mode_type'>"+drive_type_name+"</div>";
			success(input_parem);

		});

		request.fail(function( jqXHR, textStatus ) {
			alert( "Request failed: " + textStatus );
		});
	}


	LoadUserType (mode_type){
		//console.log(vehicle_uuid);
		this.LoadUserTypeList(mode_type, function (response){
			var data=response.replace(/^\s*/, '').replace(/\s*$/, '');
			if(data!='fail'){
				$( "#vehicle_only" ).hide(  );
				$( "#vehicle_select_list" ).html( data );
				//console.log("Hello");
			}else{
				$( "#js_error" ).show();
				console.log("Hello");
			}
		});

		$( "#show_submit" ).show();

	}

	LoadUserTypeList (driver_mode_code,success){

		var driver_mode_normal          = "";
		var driver_mode_prsnl           = "";
		var driver_mode_yard_move       = "";
		var driver_mode_passenger       = "";
		var driver_status_on_duty       = "";
		var driver_status_off_duty      = "";
		var type_code                   = "";

		var i                           = "";
		var val_class                   = "";
		var val_checked                 = "";
		var drive_use_type_name         = "";
		var input_parem_use             = "";
		// var driver_mode_code            = $('#mode_type label.mode_type_only input').attr("driver_mode_code");
		var mode_val_use                = "new_use ";

		var vehicle_uuid                = $('#mode_type label input').attr("vehicle_uuid");
		var cur_drvr_assig_uuid         = $('#mode_type label input').attr("cur_drvr_assig_uuid");
		var carr_uuid 		            = $('#mode_type label input').attr("carr_uuid");



		//console.log(driver_mode_code);

		var request = $.ajax({
			type:"POST",
			url: url+"use_type_select.php",
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			dataType: "json",
			data:{driver_mode_code:driver_mode_code,mode_value:mode_val_use}
		});
		request.done(function(response) {

			$.each(response["driver_use_type"], function (ind, useType) {
				if (i == 0) {
					val_checked = " checked";
					val_class = " active btn-primary";
				} else {
					val_checked = "";
					val_class = " btn-default";
				}

				/// assigning the values for the logical flags
				driver_mode_normal = useType["driver_mode_default_normal"];
				driver_mode_prsnl = useType["driver_mode_auth_prsnl_cmv"];
				driver_mode_yard_move = useType["driver_mode_yard_move"];
				driver_mode_passenger = useType["driver_mode_passenger"];
				driver_status_on_duty = useType["driver_status_on_duty"];
				driver_status_off_duty = useType["driver_status_off_duty"];
				type_code = useType["type_code"];


				//the use type buttons will be show/hidden based on the different model type code selected by the user//


				drive_use_type_name = drive_use_type_name + '<label class="btn ' + val_class + ' change_use_type_only"><input type="radio"  vehicle_uuid="' + vehicle_uuid + '"  cur_drvr_assig_uuid="' + cur_drvr_assig_uuid + '"  carr_uuid="' + carr_uuid + '" driver_mode_code="' + driver_mode_code + '" driver_mode_prsnl="' + driver_mode_prsnl + '" driver_mode_yard_move="' + driver_mode_yard_move + '" driver_mode_passenger="' + driver_mode_passenger + '" driver_status_on_duty="' + driver_status_on_duty + '" driver_status_off_duty="' + driver_status_off_duty + '"  driver_use_type_code="' + useType["type_code"] + '"  name="driver_status" ' + val_checked + ' value="' + useType["type_code"] + '"/>' + useType["type_name"] + "</label>";
			
				i++;
			});
    



			input_parem_use = "<div class='btn-group' data-toggle='buttons' id='use_type'>"+drive_use_type_name+"</div>";
			// input_parem_use = "Testing";
			success(input_parem_use);
		});
		request.fail(function( jqXHR, textStatus ) {
			alert( "Request failed: " + textStatus );
		});

	}

	/**
	 * This section is TRIGGERED when a CREATES A NEW ASSIGNMENT.
	 * @param {function} SubmitValue                -   POSTS VALUE onto the DATABASE
	 *
	 * @param {string} vehicle_uuid_use             -   Is the VEHICLE  UUID of the VEHICLE DRIVER CHOSE
	 * @param {string} carr_uuid 		            -   Is the CARRIER UUID of the carrier of the VEHICLE DRIVER
	 * @param {string} cur_drvr_assig_uuid_use      -   Determines if that vehicle is assigned to other DRIVER's. It determines the MULTIPLE DETECT situations
	 * @param {string} driver_use_type_code_use 	-	Is the use type that DRIVER used that VEHICLE for
	 * @param {BINARY} driver_mode_normal_use 	    - 	Is flag for NORMAL_DEFAULT
	 * @param {BINARY} driver_mode_prsnl_use		- 	Is flag for PERSONAL_USE
	 * @param {BINARY} driver_mode_yard_move_use    -   Is flag for YARD_MOVE
	 * @param {BINARY} driver_mode_passenger_use    -   Is flag for PASSENGER
	 * @param {BINARY} driver_status_on_duty_use    -   Is flag for ON_DUTY
	 * @param {BINARY} driver_status_off_duty_use   -   Is flag for OFF_DUTY
	 * @param {string} driver_mode_code             -   Is the mode type DRIVER used that VEHICLE for
	 */

	SubmitValue(){

		var vehicle_uuid_use            = $("#use_type label input").attr("vehicle_uuid");
		var carr_uuid                   = $("#use_type label input").attr("carr_uuid");

		var cur_drvr_assig_uuid_use     = $("#use_type label input").attr("cur_drvr_assig_uuid");
		var driver_use_type_code_use    = $("#use_type label input").attr("driver_use_type_code");

		console.log(vehicle_uuid_use);

		var driver_mode_normal_use      = $("#use_type label input").attr("driver_mode_normal");
		var driver_mode_prsnl_use       = $("#use_type label input").attr("driver_mode_prsnl");
		var driver_mode_yard_move_use   = $("#use_type label input").attr("driver_mode_yard_move");
		var driver_mode_passenger_use   = $("#use_type label input").attr("driver_mode_passenger");

		var driver_status_on_duty_use   = $("#use_type label input").attr("driver_status_on_duty");
		var driver_status_off_duty_use  = $("#use_type label input").attr("driver_status_off_duty");
		var driver_mode_code            = $("#use_type label input").attr("driver_mode_code");

		console.log(driver_status_on_duty_use);
		console.log(driver_mode_code);


        /*
         * Posting the MODE, USE TYPE AND DUTY CYCLE ONTO THE DATABASE
         */
		$.ajax({
			type:"POST",
			url: url+"continue_post.php",
			data:{driver_mode_code:driver_mode_code,driver_mode_yard_move:driver_mode_yard_move_use,driver_mode_passenger:driver_mode_passenger_use,driver_status_on_duty:driver_status_on_duty_use,driver_status_off_duty:driver_status_off_duty_use, vehicle_uuid:vehicle_uuid_use,cur_drvr_assig_uuid:cur_drvr_assig_uuid_use,carr_uuid:carr_uuid,driver_use_type_code:driver_use_type_code_use,driver_mode_normal:driver_mode_normal_use,driver_mode_prsnl:driver_mode_prsnl_use}
		}).done(function(response) {
			var data=response.replace(/^\s*/, '').replace(/\s*$/, '');
			var incoming_variables = data.split("_");
			// console.log("What is going on here?? I am here");

			switch(incoming_variables[0]){
				case 'succeess':
					$.ajax({
						type:"POST",
						url: url+"live-tracker.php",
						data: { vehicle_uuid: vehicle_uuid_use,driver_mode_code: driver_mode_code,use_type_code: driver_use_type_code_use,cur_drvr_assig_uuid_use: cur_drvr_assig_uuid_use,assignment_uuid:incoming_variables[1]},
					}).done(function(datas) {
						//console.log(datas);
						$( "#replace_a" ).html( datas);


						$.ajax({
							type:"POST",
							url: url+"get_current_info.php",
							contentType: "application/json; charset=utf-8",
							dataType: "json"
						}).done(function(response) {
							$( "#last_status" ).html( '<strong>'+response["driver_current_info"]['driver_status_name']+'</strong> on Mode: <strong>'+ response["driver_current_info"]['driver_mode_name']+'</strong> as <strong>'+response["driver_current_info"]['driver_use_type_name']+'</strong> use type');

							if(response["driver_current_info"]['driver_status_code']=='ON_DUTY'){
								$( "#show_status_time_line" ).html('<a href="javascript:void(0)" onclick="DriverInitialize.OffDuty();" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-off" duty_cycle="'+response["driver_current_info"]["driver_duty_cycle_uuid"]+'" mode_cycle="'+response["driver_current_info"]["driver_mode_cycle_uuid"]+'" driver_profile_uuid="'+response["driver_current_info"]["driver_profile_uuid"]+'" use_type_cycle="'+response["driver_current_info"]["driver_vehicle_use_type_uuid"]+'"> </span> GO OFF DUTY </a>');
							}else{}

						});

					});
					break;
			}

		});

	}

	/**
	 * This section is DRIVER CONTINUES TO THE TIME LINE
	 */

	ContinueTracker() {
		var driver_mode_code                    = $("a#off_duty  span").attr("driver_mode_code");
		var vehicle_uuid                        = $("a#off_duty  span").attr("vehicle_uuid");
		var driver_use_type_code_use            = $("a#off_duty  span").attr("use_type_code");
		// var cur_drvr_assig_uuid_use           = $("a#off_duty  span").attr("vehicle_uuid");
		var vehicle_assignment                  = $("a#off_duty  span").attr("vehicle_assignment");


		//console.log(driver_mode_code);
		$.ajax({
			type:"POST",
			url: url+"live-tracker.php",
			data: { vehicle_uuid: vehicle_uuid,driver_mode_code: driver_mode_code,use_type_code: driver_use_type_code_use,assignment_uuid:vehicle_assignment},
		}).done(function(datas) {
			//console.log(datas);
			$( "#replace_a" ).html( datas);


			$.ajax({
				type:"POST",
				url: url+"get_current_info.php",
				contentType: "application/json; charset=utf-8",
				dataType: "json"
			}).done(function(response) {
				$( "#last_status" ).html( '<strong>'+response["driver_current_info"]['driver_status_name']+'</strong> on Mode: <strong>'+ response["driver_current_info"]['driver_mode_name']+'</strong> as <strong>'+response["driver_current_info"]['driver_use_type_name']+'</strong> use type');

				if(response["driver_current_info"]['driver_status_code']=='ON_DUTY'){
					$( "#show_status_time_line" ).html('<a href="javascript:void(0)" onclick="DriverInitialize.OffDuty();"  class="btn btn-info btn-xs"><span class="glyphicon glyphicon-off" duty_cycle="'+response["driver_current_info"]["driver_duty_cycle_uuid"]+'" mode_cycle="'+response["driver_current_info"]["driver_mode_cycle_uuid"]+'" driver_profile_uuid="'+response["driver_current_info"]["driver_profile_uuid"]+'" use_type_cycle="'+response["driver_current_info"]["driver_vehicle_use_type_uuid"]+'"> </span> GO OFF DUTY </a>');
				}else{}

			});

		});
	}
	/**
	 * This section is DRIVER GOES OFF DUTY. When DRIVER goes OFF_DUTY; all the current exisiting DUTY_CYLE, MODE_CYCLE,USE_TYPE_CYCLE, VEHICLE_ASSIGNMENT_CYCLE are closed.
	 */
	OffDuty(){

		var duty_cycle                = $("a#off_duty span").attr("duty_cycle");
		var mode_cycle                = $("a#off_duty span").attr("mode_cycle");
		var use_type_cycle 		      = $("a#off_duty span").attr("use_type_cycle");
		var driver_profile_uuid 	  = $("a#off_duty span").attr("driver_profile_uuid");
		var driver_mode_code          = 'OFF_DUTY';

		// console.log(duty_cycle);

		$.ajax({
			type:"POST",
			url: url+"off_duty_close_all.php",
			dataType: "json",
			data: {duty_cycle:duty_cycle,mode_cycle:mode_cycle,use_type_cycle:use_type_cycle,driver_profile_uuid:driver_profile_uuid,driver_mode_code:driver_mode_code}

		}).done(function(response) {
			$("#list_v").show();
			$( "#vehicle_select_list" ).html( vehicle_list_parem );

		});
	}

    /*
     * This function is triggered when DRIVER just wants to be ON_DUTY. In such cases, only the ON_DUTY_CYCLE is instantiated
     */
	OnlineOnly(){
		var use_type        = 'ON_DUTY';
		var vehicle_uuid    = 'NOT_APPLICABLE';
		//var event_uuid      = $("#on_line_only span a span").attr("event_uuid");

		var request = $.ajax({
			type:"POST",
			url: url+"on_duty_only.php",
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			//dataType: "json",
			//data: { event_uuid: event_uuid},
		});
		request.done(function(response) {
			$.ajax({
				type:"POST",
				url: url+"live-tracker.php",
				data: { vehicle_uuid: vehicle_uuid,use_type_code: use_type},
			}).done(function(datas) {
				//console.log(datas);
				$( "#replace_a" ).html( datas);
			});
		});
		request.fail(function( jqXHR, textStatus ) {
			alert( "Request failed: " + textStatus );
		});
	}

    /*
     * This function is triggered when DRIVER just wants to be OFF_DUTY. In such cases, only the OFF_DUTY_CYCLE is closed
     */

	GoOffDuty(){
		var use_type        = 'OFF_DUTY';
		var request = $.ajax({
			type:"POST",
			url: url+"off_duty_option.php",
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			//dataType: "json",
			//data: { event_uuid: event_uuid},
		});
		request.done(function(response) {
			var data=response.replace(/^\s*/, '').replace(/\s*$/, '');
			if(data=='success'){
				window.location.href='http://elog.intellifuel.net/driver/'
			}

		});
		request.fail(function( jqXHR, textStatus ) {
			alert( "Request failed: " + textStatus );
		});
	}

	SetUseType(use_type_code,driver_mode_code){

		//var driver_mode_code =  $(".time_line input").attr("driver_mode_code");
		//var current_use_type =  $(".time_line input").attr("driver_use_type_code");

		$.ajax({
			type:"POST",
			url: url+"change_use_type.php",
			//data:{driver_mode_code:driver_mode_code,driver_mode_yard_move:driver_mode_yard_move_use,driver_mode_passenger:driver_mode_passenger_use,driver_status_on_duty:driver_status_on_duty_use,driver_status_off_duty:driver_status_off_duty_use, vehicle_uuid:vehicle_uuid_use,cur_drvr_assig_uuid:cur_drvr_assig_uuid_use,carr_uuid:carr_uuid_use,driver_use_type_code:driver_use_type_code_use,driver_mode_normal:driver_mode_normal_use,driver_mode_prsnl:driver_mode_prsnl_use}
			data:{driver_mode_code:driver_mode_code,current_use_type:use_type_code}

		}).done(function(response) {
		});
	}

	VehicleAssignmentOnly(){
		var vehicle_uuid                = $('#vehicle_only a span').attr("vehicle_uuid");
		var carr_uuid 		            = $('#vehicle_only a span').attr("carr_uuid");
		var curr_dri_assgn	            = $('#vehicle_only a span').attr("vehicle_current_driver_assignment_total");
		var use_type	                = $('#vehicle_only a span').attr("use_type");

		$.ajax({
			type:"POST",
			url: url+"vehicle_assignment.php",
			//data:{driver_mode_code:driver_mode_code,driver_mode_yard_move:driver_mode_yard_move_use,driver_mode_passenger:driver_mode_passenger_use,driver_status_on_duty:driver_status_on_duty_use,driver_status_off_duty:driver_status_off_duty_use, vehicle_uuid:vehicle_uuid_use,cur_drvr_assig_uuid:cur_drvr_assig_uuid_use,carr_uuid:carr_uuid_use,driver_use_type_code:driver_use_type_code_use,driver_mode_normal:driver_mode_normal_use,driver_mode_prsnl:driver_mode_prsnl_use}
			data:{vehicle_uuid:vehicle_uuid,carrier_uuid:carr_uuid,current_driver_assignment:curr_dri_assgn,use_type:use_type}

		}).done(function(response) {
		});
	}



}


/*
 *Initializing the class
 */
DriverInitialize = new Driver();

