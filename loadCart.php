<?php
/**
 * Created by PhpStorm.
 * User: ash
 * Date: 01/15/17
 * Time: 2:05 PM
 */
require("./configFront.php");
require("./application/ShoppingCart.php");


/*
 * This page will list all items on the cart.
 * @param {string} $serial_number ; is the serial number posted by the user from the search text box on the APP;
 */

$ShoppingCart       = new ShoppingCart();

$ListCart            = $ShoppingCart->ListItems(SESSION);

$count_rows          = $ShoppingCart->GetCount(SESSION);
if(empty($ListCart)){
    $value = false;
}else{
    $value = true;
}
if($_POST['GetCount']==0){
    $returnPack       = json_encode(array(
            "success"=>$value,
            "cart_items"=>$ListCart
        )
    );
}else{
    $returnPack       = json_encode(array(
            "success"=>$value,
            "cart_items"=>$count_row
        )
    );
}

print_r($returnPack);
